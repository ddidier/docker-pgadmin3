#!/bin/bash

#
# Utilities to include in scripts.
#
# @version 0.1.2
# @author David DIDIER
# @see https://gitlab.com/ddidier/docker-tools
#

TXT_BLACK='\e[0;30m'
TXT_RED='\e[0;31m'
TXT_GREEN='\e[0;32m'
TXT_YELLOW='\e[0;33m'
TXT_BLUE='\e[0;34m'
TXT_PURPLE='\e[0;35m'
TXT_CYAN='\e[0;36m'
TXT_WHITE='\e[0;37m'

BOLD_BLACK='\e[1;30m'
BOLD_RED='\e[1;31m'
BOLD_GREEN='\e[1;32m'
BOLD_YELLOW='\e[1;33m'
BOLD_BLUE='\e[1;34m'
BOLD_PURPLE='\e[1;35m'
BOLD_CYAN='\e[1;36m'
BOLD_WHITE='\e[1;37m'

UND_BLACK='\e[4;30m'
UND_RED='\e[4;31m'
UND_GREEN='\e[4;32m'
UND_YELLOW='\e[4;33m'
UND_BLUE='\e[4;34m'
UND_PURPLE='\e[4;35m'
UND_CYAN='\e[4;36m'
UND_WHITE='\e[4;37m'

BG_BLACK='\e[40m'
BG_RED='\e[41m'
BG_GREEN='\e[42m'
BG_YELLOW='\e[43m'
BG_BLUE='\e[44m'
BG_PURPLE='\e[45m'
BG_CYAN='\e[46m'
BG_WHITE='\e[47m'

TXT_RESET='\e[0m'

#
# Print a title at level 1.
#
# $1 - the title to print.
#
function print_h1() {
    declare -r title="${1}"
    printf "${BOLD_YELLOW}%s${TXT_RESET}\n" "${title}"
}

#
# Print a title at level 2.
#
# $1 - the title to print.
#
function print_h2() {
    declare -r title="${1}"
    printf "${UND_YELLOW}%s${TXT_RESET}\n" "${title}"
}

#
# Print an error message.
#
# $1 - the message to print.
#
function print_error() {
    declare -r message="${1}"
    printf "${BOLD_RED}✗ ERROR: %s${TXT_RESET}\n" "${message}"
}

#
# Print an error message and exit.
#
# $1 - the message to print.
#
function print_fatal() {
    declare -r message="${1}"
    printf "${BOLD_RED}✗ FATAL: %s${TXT_RESET}\n" "${message}"
    exit 1
}

#
# Check that the parameter is a valid semantic version, i.e.
#
# $1 - the parameter to check.
#
function check_semantic_version() {
    declare -r tag="${1}"
    if [[ ! "${tag}" =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
        print_fatal "Tag '${tag}' is not a semantic version"
    fi
}

#
# Get a Docker Hub JWT token.
#
# $1 - the Docker Hub username.
# $2 - the Docker Hub password.
#
# @return the Docker Hub JWT token.
#
function get_token() {
    declare -r username="${1}"
    declare -r password="${2}"

    curl -s \
         -H "Content-Type: application/json" \
         -X POST \
         -d '{ "username": "'${username}'", "password": "'${password}'" }' \
         "https://hub.docker.com/v2/users/login/" \
    | jq -r .token
}

#
# Pushes README.md content to Docker Hub.
#
# $1 - The image name.
# $2 - The JWT token.
#
function push_readme() {
    declare -r image="${1}"
    declare -r token="${2}"

    local code=$( \
        jq \
            -n --arg msg "$(<README.md)" \
            '{"registry": "registry-1.docker.io", "full_description": $msg }' \
        | curl \
            -s -o /dev/null  -L -w "%{http_code}" \
            https://cloud.docker.com/v2/repositories/"${image}"/ \
            -d @- -X PATCH \
            -H "Content-Type: application/json" \
            -H "Authorization: JWT ${token}" \
    )

    if [[ "${code}" = "200" ]]; then
        printf "Pushed README to Docker Hub\n"
    else
        print_fatal "Unable to push README to Docker Hub, response code: ${code}"
    fi
}
