# NDD Docker pgAdmin

## Version 0.5.1

- Transfer to GitLab
- Add a reference to `ddidier/pgadmin4`

## Version 0.5.0

- Update pgAdmin version to `1.22.2`
- Fix entrypoint

## Version 0.4.0

- Add backup/restore with pg_dump

## Version 0.3.0

- Update pgAdmin version to `1.22.1`
- Fix sudoers
- Refactor Dockerfile

## Version 0.2.0

- Use the latest official pgAdmin release

## Version 0.1.0

- Initial commit
