# NDD Docker pgAdmin3

<!-- MarkdownTOC -->

1. [Introduction](#introduction)
1. [Installation](#installation)
    1. [From source](#from-source)
    1. [From Docker Hub](#from-docker-hub)
1. [Usage](#usage)
1. [Configuration](#configuration)

<!-- /MarkdownTOC -->



<a id="introduction"></a>
## Introduction

A Docker image for [pgAdmin3](https://www.pgadmin.org/).

References:

- image on Docker Hub : https://hub.docker.com/r/ddidier/pgadmin
- sources on GitLab : https://gitlab.com/ddidier/docker-pgadmin3

A Docker image for [pgAdmin4  in server mode is available too](https://hub.docker.com/r/ddidier/pgadmin4/)!

The image is based upon the official [debian:jessie](https://hub.docker.com/_/debian/) image and the official [pgAdmin3 release](https://wiki.postgresql.org/wiki/Apt).



<a id="installation"></a>
## Installation

<a id="from-source"></a>
### From source

```
git clone git@gitlab.com:ddidier/docker-pgadmin3.git
cd docker-pgadmin3
docker build -t ddidier/pgadmin .
```

<a id="from-docker-hub"></a>
### From Docker Hub

```
docker pull ddidier/pgadmin
```



<a id="usage"></a>
## Usage

pgAdmin3 will be executed by the `pgadmin3` user which is created by the Docker entry point. You **must** pass to the container the environment variable `USER_ID` set to the UID of the user the files will belong to. For example ``-e USER_ID=$UID``.

The settings are stored in the container in the directory `/home/pgadmin3`. You have multiple choices to keep them:

- never delete your container
- use a data volume e.g. `-v $HOST_DATA_DIR:/home/pgadmin3`
- use a data volume container e.g. `--volumes-from pgadmin-data`

If you store these data in a directory like `~/.docker-data/<CONTAINER_NAME>`, you can run this image using:

- the legacy networking:

```bash
docker run --rm                              \
    -v /tmp/.X11-unix:/tmp/.X11-unix         \
    -v ~/.docker-data/pgadmin:/home/pgadmin3 \
    -e DISPLAY=$DISPLAY                      \
    -e USER_ID=$UID                          \
    --link postgres:postgres                 \
    --name pgadmin                           \
    ddidier/pgadmin
```

- a custom network:

```bash
docker run --rm                              \
    -v /tmp/.X11-unix:/tmp/.X11-unix         \
    -v ~/.docker-data/pgadmin:/home/pgadmin3 \
    -e DISPLAY=$DISPLAY                      \
    -e USER_ID=$UID                          \
    --net=mynetwork                          \
    --name pgadmin                           \
    ddidier/pgadmin
```



<a id="configuration"></a>
## Configuration

To enable backup/restore, go in `File > Options > Browser > Binary paths` and set `PG bin path` to `/usr/lib/postgresql/9.6/bin`.
